# frozen_string_literal: true

require_relative "colab_grouper/version"
require 'uri'
require 'net/http'
require 'openssl'
require 'cgi'
require 'base64'
require 'json'

module ColabGrouper
  class Error < StandardError; end

  class Group
    def initialize(svc_duid: , svc_password: , base_grouper_path: "https://groups.oit.duke.edu/grouper-ws/servicesRest/json/v2_5_000", read_timeout: 60, default_open_timeout: 10)
      @base_grouper_path = base_grouper_path
      @read_timeout = read_timeout
      @default_open_timeout = default_open_timeout
      @auth = Base64.strict_encode64("#{svc_duid}:#{svc_password}")
      @svc_duid =svc_duid
      @svc_password = svc_password
    end

    #add a group or individual to group. grouper_id is typically duid for the individual
    def add_member(group_path, grouper_id:, raw: false)
      path = "/groups/#{CGI.escape(group_path)}/members/#{grouper_id}"
      response = put_request(path: path)
      return response if raw
      JSON.parse(response.body)&.dig("WsAddMemberLiteResult","resultMetadata","resultCode")    
    end

  
    # get all membership of a group
    def members(group_path,  raw: false, includeGroupIds: false)
      path = "groups/#{CGI.escape(group_path)}/members"
      response = get_request(path: path)
      
      return response if raw
      grouper_ids = JSON.parse(response.read_body)&.dig("WsGetMembersLiteResult","wsSubjects")&.map{|i| i["id"]}
      return grouper_ids if includeGroupIds
      grouper_ids.keep_if {|i| i.length <= 10 } 
    end

    def memberships(duid, raw:false)
      path = "subjects/#{CGI.escape(duid)}/memberships"
      response = get_request(path: path)
      
      return response if raw
      JSON.parse(response.body)&.dig("WsGetMembershipsResults","wsGroups")&.map{|i| i["name"]}
    end


    #add a group or individual to group. grouper_id is typically duid for the individual
    def remove_member(group_path, grouper_id:, raw: false)
      path = "/groups/#{CGI.escape(group_path)}/members/#{grouper_id}"
      response = delete_request(path: path)
      return response if raw
      JSON.parse(response.body)&.dig("WsAddMemberLiteResult","resultMetadata","resultCode")    
    end
  
    private 

    def delete_request(path:, body: nil)
      url = URI("#{@base_grouper_path}/#{path}")
      http = http_settings(url)
      request = Net::HTTP::Delete.new(url)
      request["Authorization"] = "Basic #{@auth}"
      request.body = body.to_json

      response = http.request(request)
      response
    end 

    def get_request(path:)
      url = URI("#{@base_grouper_path}/#{path}")
      http = http_settings(url)
      request = Net::HTTP::Get.new(url)
      request["Authorization"] = "Basic #{@auth}"

      response = http.request(request)
      response
    end 

    # global settings for all http requests
    def http_settings(url)
      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      http.read_timeout = @read_timeout
      http.keep_alive_timeout = @keep_alive_timeout
      http
    end
    

    def put_request(path:, body: nil)
      url = URI("#{@base_grouper_path}/#{path}")
      http = http_settings(url)
      request = Net::HTTP::Put.new(url)
      request["Authorization"] = "Basic #{@auth}"
      request.body = body.to_json

      response = http.request(request)
      response
    end 
  end
end
